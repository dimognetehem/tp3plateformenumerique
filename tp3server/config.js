const config = {
    db: {
        connectionLimit: 10,
        host: "localhost",
        user: "root",
        password: "",
        database: "tp3nodejs",
        connectionTimeout: 6000,
    },
    listPerPage: 10
};
module.exports = config;