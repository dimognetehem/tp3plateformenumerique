const express = require('express')
let router  = express.Router();
const musicServ =  require('../services/music.service')

router.get('/', async function(req, res, next) {
    try{
        res.json(await musicServ.getListOfMusics(req.query.page));
    }catch(err){
        console.error(`Error while getting musics `, err.message)
        next(err);
    }
});

router.get('/music/:id', async function(req, res, next){
    console.log(req.params.id);

    try{
        res.json(await musicServ.getMusic(req.params.id));
    } catch (err){
        console.error(`Error while getting music`, err.message);
        next(err);
    }
    
});

router.put('/update/:id', async function(req, res, next) {
    try{
        res.json(await musicServ.updateMusic(req.params.id, req.body));
    }catch(error){
        console.error("Error while creating music : ", error)
        next(error)
    }
});

router.delete('/delete/:id', async function(req, res, next) {
    try{
        res.json(await musicServ.deleteMusic(req.params.id));
    }catch(err){
        console.error(`Error while deleting music `, err.message)
        next(err);
    }
});

router.post('/add', async function(req, res, next) {
    try{
        res.json(await musicServ.addMusic(req.body));
    }catch(error){
        console.error("Error while creating music : ", error)
        next(error)
    }
})


module.exports = router