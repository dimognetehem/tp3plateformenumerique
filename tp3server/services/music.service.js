const db = require('./db.service')
const helper = require('../helper')
const config = require('../config')


async function addMusic(music) {
    try {
      const query = `
        INSERT INTO music(id, titre, nomArt, featArt, releasedDate, lyrics, url, pochette, prix)
        VALUES (NULL, ?, ?, ?, current_timestamp(), ?, ?, ?, ?);
      `;
  
      const values = [
        music.titre,
        music.nomArt,
        music.featArt,
        music.lyrics,
        music.url,
        music.pochette,
        music.prix
      ];
  
      const rows = await db.query(query, values);
      const data = helper.emptyOrRows(rows);
  
      return data;
    } catch (error) {
      console.error('Erreur lors de l\'enregistrement de la musique :', error);
      throw error; // Vous pouvez gérer l'erreur comme vous le souhaitez
    }
}

async function getListOfMusics() {
    const rows = await db.query(
        `SELECT * FROM music`
        );
    const data = helper.emptyOrRows(rows);


    return data
}

async function getMusic(id) {
    const rows = await db.query(
        `SELECT * FROM music WHERE id = ${id}`
    );
    const data = helper.emptyOrRows(rows);

    return data
}

async function deleteMusic(id) {
    const rows = await db.query(
        `DELETE FROM music where id = ${id}`
    )
    const data = helper.emptyOrRows(rows)
    return {
        data
    }
}

async function updateMusic(id, music) {
    const query = `
      UPDATE music
      SET titre = ?, nomArt = ?, featArt = ?, lyrics = ?, url = ?, pochette = ?, prix = ?
      WHERE id = ?;
    `;
  
    const values = [
      music.titre,
      music.nomArt,
      music.featArt,
      music.lyrics,
      music.url,
      music.pochette,
      music.prix,
      id
    ];
  
    const rows = await db.query(query, values);
    const data = helper.emptyOrRows(rows);
  
    return data;
  }
  
module.exports = {
    addMusic,
    getListOfMusics,
    getMusic,
    deleteMusic,
    updateMusic
    }