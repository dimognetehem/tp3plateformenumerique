const express = require("express")
const app = express()
const https = require("https")
const fs = require('fs');
const cors = require("cors");
const multer = require('multer');
const path = require('path');

const port = 3000;
const musicRoute = require("./routes/musicRoutes");

const options = {
  key: fs.readFileSync('./certifs/private-key.pem'),
  cert: fs.readFileSync('./certifs/certificate.pem'),
}

app.use(cors());
app.use(express.json())
app.use(
    express.urlencoded({
        extended: true,
    })
)
app.get("/", (req, res)=>{
    res.json({message: "OK"});
});


const storage = multer.diskStorage({
  destination: './assets/medias',
  filename: function (req, file, cb) {
    cb(null, `${file.originalname}`);
  }
});

const upload = multer({storage : storage});


app.post('/upload', upload.fields([{ name: 'file', maxCount: 1 }, { name: 'img', maxCount: 1 }]), (req, res) => {
  const audioFile = req.files['file'][0];
  console.log(audioFile);
  const albumArtFile = req.files['img'][0];
  console.log(albumArtFile);

  // Renvoyez une réponse appropriée au client
  res.status(200).json({ message: 'Fichiers téléchargés avec succès' });
});


app.use("/musics", musicRoute);


app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    res.status.statusCode.json({message: err.message})
    return ;
});

// Serve static files (images, audio, etc.)
app.use('/assets/medias', express.static('./assets/medias'));

/* const server = https.createServer(options, app);

server.listen(port, '192.168.8.100',() => {
  console.log(`Le serveur est en cours d'écoute sur https://192.168.8.100:${port}`);
}); */

app.listen(port, '192.168.8.100',() => {
  console.log(`Le serveur est en cours d'écoute sur http://192.168.8.100:${port}`);
});