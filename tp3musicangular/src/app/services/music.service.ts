import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Music} from "../models/music";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  private apiUrl = 'http://192.168.8.100:3000/musics';

  constructor(private http: HttpClient) { }

  getListOfMusic(){

    return this.http.get(this.apiUrl);
  }

  addMusic(music: Music): Observable<Music> {
    return this.http.post<Music>(this.apiUrl+"/add", music);
  }
  deleteMusic(id: number){
    return this.http.delete(this.apiUrl+"/delete/"+id)
  }
  updateMusic(id: number, music : Music){
    return this.http.put(this.apiUrl+"/update/"+id, music)
  }

  uploadAudio(file: FormData) {
    return this.http.post("http://192.168.8.100:3000/upload", file);
  }

}
