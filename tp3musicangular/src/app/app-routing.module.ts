import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AccueilComponent} from "./components/accueil/accueil.component";
import {SavemusicComponent} from "./components/savemusic/savemusic.component";
import {UpdatemusicComponent} from "./components/updatemusic/updatemusic.component";

const routes: Routes = [
  {path : '', component: AccueilComponent},
  {path : 'accueil', component: AccueilComponent},
  {path : 'savemusic', component: SavemusicComponent},
  {path : 'updatemusic/:id', component: UpdatemusicComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
