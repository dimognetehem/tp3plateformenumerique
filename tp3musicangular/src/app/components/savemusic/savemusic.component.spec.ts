import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavemusicComponent } from './savemusic.component';

describe('SavemusicComponent', () => {
  let component: SavemusicComponent;
  let fixture: ComponentFixture<SavemusicComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SavemusicComponent]
    });
    fixture = TestBed.createComponent(SavemusicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
