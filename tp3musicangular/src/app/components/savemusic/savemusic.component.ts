import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MusicService} from "../../services/music.service";
import {HttpClient} from "@angular/common/http";
import {Music} from "../../models/music";

@Component({
  selector: 'app-savemusic',
  templateUrl: './savemusic.component.html',
  styleUrls: ['./savemusic.component.css']
})
export class SavemusicComponent {

  musicForm: FormGroup;

  saveSucceed: boolean = false;

  saveFailed: boolean = false;

  @ViewChild('audioFile', {static: false}) audioFile!: ElementRef;

  @ViewChild('imgFile', {static: false}) imgFile!: ElementRef;

  music: Music = {
    id: 0,
    titre: '',
    nomArt: '',
    featArt: '',
    releasedDate: new Date(),
    lyrics: '',
    url: '',
    pochette: '',
    prix: 0
  };



  constructor(
    private fb: FormBuilder,
    private musicServ: MusicService,
    private http: HttpClient
  ) {
    this.musicForm = this.fb.group({
      titre: ['', Validators.required],
      nomArt: ['', Validators.required],
      featArt: '',
      releasedDate: new Date(),
      lyrics: ['', Validators.required],
      audioFile: ['', Validators.required],
      pochette: ['', Validators.required],
      prix: ['', [Validators.required, Validators.min(0)]]
    });
  }


  onSubmit() {
    if(this.musicForm.valid){
      this.saveFailed = true;

      const audioBlob = this.audioFile.nativeElement.files[0];

      const imgBlob = this.imgFile.nativeElement.files[0];
      console.log(audioBlob);
      const file = new FormData();
      file.set('file', audioBlob);
      file.set('img', imgBlob);

      this.musicServ.uploadAudio(file).subscribe(res =>{
        console.log(res);
      });
      // @ts-ignore
      this.music.titre = this.musicForm.get('titre').value;

      // @ts-ignore
      this.music.nomArt = this.musicForm.get('nomArt').value;

      // @ts-ignore
      this.music.featArt = this.musicForm.get('featArt').value;

      // @ts-ignore
      this.music.lyrics = this.musicForm.get('lyrics').value;

      // @ts-ignore
      this.music.pochette = "https://192.168.8.100:3000/assets/medias/"+imgBlob.name;

      // @ts-ignore
      this.music.prix = this.musicForm.get('prix').value;

      // ... Ajoutez d'autres champs comme ceci
      // @ts-ignore
      this.musicServ.uploadAudio(this.musicForm.get('audioFile'));

      // Ajoutez le fichier audio au FormData
      // @ts-ignore
      this.music.url = "https://192.168.8.100:3000/assets/medias/"+audioBlob.name;
      console.log(this.music);

      this.musicServ.addMusic(this.music).subscribe(response => {
        console.log('Musique enregistrée avec succès:', response);

      });

      this.musicForm.reset();

      this.saveSucceed = true;

      this.saveFailed = false;

    }
    else {
      console.log("Formulaire non valide");
      this.saveFailed = true;
    }

  }


}
