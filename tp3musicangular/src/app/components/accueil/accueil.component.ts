import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MusicService} from "../../services/music.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Music} from "../../models/music";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit{

  all:any[] = [];

  constructor(
    private musicServ: MusicService,
  ) {

  }


  ngOnInit(){
    // setInterval(()=>{
      this.musicServ.getListOfMusic().subscribe((res: any) => {
        this.all = res;
        console.log(this.all);
      });
    // }, 5000);

  }

  ajusterUrlPochette(url: string): string {
    // Supprimer le 's' de 'https'
    return url.replace('https', 'http');
  }

  deleteMusic(id: number){
    confirm('Etes-vous sûr de vouloir supprimer cette musique ?');

    this.musicServ.deleteMusic(id).subscribe((res:any)=>{ console.log(res)});

  }



}
