import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Music} from "../../models/music";
import {MusicService} from "../../services/music.service";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-updatemusic',
  templateUrl: './updatemusic.component.html',
  styleUrls: ['./updatemusic.component.css']
})
export class UpdatemusicComponent implements OnInit{

  idSong:number|any;

  updateForm: FormGroup;

  updateSucceed: boolean = false;

  updateFailed: boolean = false;

  @ViewChild('audioFile', {static: false}) audioFile!: ElementRef;

  @ViewChild('imgFile', {static: false}) imgFile!: ElementRef;

  music: Music = {
    id: 0,
    titre: '',
    nomArt: '',
    featArt: '',
    releasedDate: new Date(),
    lyrics: '',
    url: '',
    pochette: '',
    prix: 0
  };

  test!:any;



  constructor(
    private fb: FormBuilder,
    private musicServ: MusicService,
    private http: HttpClient,
    private router: ActivatedRoute
  ) {
    this.idSong = this.router.snapshot.paramMap.get("id");
    this.updateForm = this.fb.group({
      titre: ['', Validators.required],
      nomArt: ['', Validators.required],
      featArt: '',
      releasedDate: new Date(),
      lyrics: ['', Validators.required],
      audioFile: ['', Validators.required],
      pochette: ['', Validators.required],
      prix: ['', [Validators.required, Validators.min(0)]]
    });

  }

   ngOnInit() {

     this.musicServ.getListOfMusic().subscribe((reponse: any) => {

       // @ts-ignore
       this.test = reponse.filter(a => a.id == this.idSong);

       console.log(this.test);

     });


  }


  onSubmit() {

    if(this.updateForm.valid){
      this.updateFailed = true;

      const audioBlob = this.audioFile.nativeElement.files[0];

      const imgBlob = this.imgFile.nativeElement.files[0];
      console.log(audioBlob);
      const file = new FormData();
      file.set('file', audioBlob);
      file.set('img', imgBlob);

      this.musicServ.uploadAudio(file).subscribe(res =>{
        console.log(res);
      });
      // @ts-ignore
      this.music.titre = this.updateForm.get('titre').value;

      // @ts-ignore
      this.music.nomArt = this.updateForm.get('nomArt').value;

      // @ts-ignore
      this.music.featArt = this.updateForm.get('featArt').value;

      // @ts-ignore
      this.music.lyrics = this.updateForm.get('lyrics').value;

      // @ts-ignore
      this.music.pochette = "https://192.168.8.100:3000/assets/medias/"+imgBlob.name;

      // @ts-ignore
      this.music.prix = this.updateForm.get('prix').value;

      // ... Ajoutez d'autres champs comme ceci
      // @ts-ignore
      this.musicServ.uploadAudio(this.updateForm.get('audioFile'));

      // Ajoutez le fichier audio au FormData
      // @ts-ignore
      this.music.url = "https://192.168.8.100:3000/assets/medias/"+audioBlob.name;
      console.log(this.music);
      // formData.append('audioFile', audioFile);
      // formData.append('audioFile', audioFile.files[0], audioFile.files[0].name);

      this.musicServ.updateMusic(this.idSong, this.music).subscribe(response => {
        console.log('Musique modifiée avec succès:', response);
        // Réinitialisez le formulaire après l'enregistrement
        // this.updateForm.reset();
      });

      this.updateForm.reset();

      this.updateSucceed = true;

      this.updateFailed = false;
    }
    else{
      console.log("Formulaire non valide");
      this.updateFailed = true;
    }

  }

}
