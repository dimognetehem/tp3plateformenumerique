import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatemusicComponent } from './updatemusic.component';

describe('UpdatemusicComponent', () => {
  let component: UpdatemusicComponent;
  let fixture: ComponentFixture<UpdatemusicComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdatemusicComponent]
    });
    fixture = TestBed.createComponent(UpdatemusicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
