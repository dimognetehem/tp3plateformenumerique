import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Music} from "../models/music";

@Injectable({
  providedIn: 'root'
})
export class MusicService {

  private apiUrl = 'https://192.168.8.100:3000/musics';

  constructor(private http: HttpClient) { }



  getListOfMusic(){

    return this.http.get(this.apiUrl);
  }

  getMusic(id: number){
    return this.http.get<Music>(this.apiUrl+"/music/"+id);
  }

}
