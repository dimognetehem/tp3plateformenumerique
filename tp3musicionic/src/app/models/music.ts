export interface Music {

  id: number;
  titre: string;
  nomArt: string;
  featArt: string;
  releasedDate: Date;
  lyrics: string;
  url: string;
  pochette: string;
  prix: number;
}
