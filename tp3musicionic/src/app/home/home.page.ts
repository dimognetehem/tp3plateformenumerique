import {Component, inject, OnInit} from '@angular/core';

import {MusicService} from "../services/music.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  all:any[] = [];
  chansonsfilter: any[] = [];
  searchControl: string = '';

  constructor(private musicServ: MusicService) {}


  ngOnInit(){
    this.musicServ.getListOfMusic().subscribe((res: any) => {
      this.all = res;

      this.chansonsfilter = res;
    });

  }

  filterSongs(query: string) {
    this.chansonsfilter = this.all.filter(d => d.titre.toLowerCase().indexOf(query) > -1);

  }

  handleInput() {
    this.filterSongs(this.searchControl.toLowerCase());
  }

  handleRefresh(event: any) {
    setTimeout(() => {
      this.musicServ.getListOfMusic().subscribe((res: any) => {
        this.all = res;

        this.chansonsfilter = res;
      });
      event.target.complete();
    }, 2000);
  }

}
