import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MusicService} from "../services/music.service";
import {Music} from "../models/music";

@Component({
  selector: 'app-detailsmusic',
  templateUrl: './detailsmusic.page.html',
  styleUrls: ['./detailsmusic.page.scss'],
})
export class DetailsmusicPage implements OnInit {

  idSong: number|any;
  music:any;
  showFullLyrics!:boolean;

  constructor(private musicServ: MusicService, private router: ActivatedRoute) {
    this.idSong = this.router.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.musicServ.getListOfMusic().subscribe((reponse: any) => {
      // @ts-ignore
      this.music = reponse.filter(a => a.id == this.idSong);

    });

  }

}
