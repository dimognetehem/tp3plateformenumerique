import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailsmusicPage } from './detailsmusic.page';

describe('DetailsmusicPage', () => {
  let component: DetailsmusicPage;
  let fixture: ComponentFixture<DetailsmusicPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DetailsmusicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
