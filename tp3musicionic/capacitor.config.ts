import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.ing4isi.dimtp3music',
  appName: 'TP3 DIMOGNE TEHEM',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
